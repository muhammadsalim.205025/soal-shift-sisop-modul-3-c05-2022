# Soal Shift Sisop Modul 3 C05 2022

## Anggota Kelompok ##

NRP | NAMA
------------- | -------------
5025201057    | Muhammad Fuad Salim
5025201112    | Naufal Ariq Putra Yosyam
5025201139    | Nabila Zakiyah Khansa' Machrus

# SOAL 1 
Mengalami kesusahan pada bagian unzip file

REVISI

A. Download dua file zip , quote.zip dan music.zip. 
```C
void download_drive(char *id, char *save)
{
    char *link = malloc(100);
    strcpy(link, "https://docs.google.com/uc?export=download&id=");
    strcat(link, id);
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"wget", "--quiet", "--no-check-certificate", link, "-O", save, NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    wait(&status);
}
```
Dan unzip file zip tersebut di dua folder yang berbeda yaitu quote dan music. Unzip ini dilakukan dengan bersamaan menggunakan thread.
```C
void *thread_unzip(void *arg)
{
    char *argv1[] = {"unzip", "-qo", "music.zip", "-d", "./music/", NULL};
    char *argv2[] = {"unzip", "-qo", "quote.zip", "-d", "./quote/", NULL};
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[0]))
    {
        if (fork() == 0)
        {
            execv("/usr/bin/unzip", argv1);
        }
        wait(NULL);
    }
    else if (pthread_equal(id, tid[1]))
    {
        if (fork() == 0)
        {
            execv("/usr/bin/unzip", argv2);
        }
        wait(NULL);
    }
    return NULL;
}
```

B. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
```C
char *base64_decode(char *encoded)
{
    char counts = 0;
    char buffer[4];
    char *decoded = malloc(strlen(encoded) * 3 / 4);
    int i = 0, p = 0;

    for (i = 0; encoded[i] != '\0'; i++)
    {
        char k;
        for (k = 0; k < 64 && base64_map[k] != encoded[i]; k++)
            ;
        buffer[counts++] = k;
        if (counts == 4)
        {
            decoded[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                decoded[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                decoded[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    decoded[p] = '\0';
    return decoded;
}
```
```c
void *thread_decode(void *arg)
{
    pthread_t id = pthread_self();
    char path1[10];
    char path2[12];
    char filename[10];
    if (pthread_equal(id, tid2[0]))
    {
        strcpy(path1, "./music/");
        strcpy(path2, "./music/%s");
        strcpy(filename, "music.txt");
    }
    else
    {
        strcpy(path1, "./quote/");
        strcpy(path2, "./quote/%s");
        strcpy(filename, "quote.txt");
    }
    DIR *dir;
    struct dirent *dp;
    dir = opendir(path1);
    FILE *file, *txt;
```

C. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
```c
 create_dir("hasil");

    if (fork() == 0)
    {
        char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    wait(NULL);

    if (fork() == 0)
    {
        char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    wait(NULL);
```

# SOAL 2
## A. REGISTER DAN LOGIN

Ketika Client dijalankan, Client akan mencoba untuk melakukan koneksi dengan server menggunakan socket. Terdapat beberapa kasus yang akan menyebabkan kegagalan dalam pengubungan server dan client yang akan mengembalikan pesan error seperti kegagalan dalam pembuatan socket yang akan mengembalikan "Failed to create socket", kegagalan dalam pencarian alamat address yang akan mengembalikan "Invalid address", dan kegagalan penyambungan koneksi yang akan mengembalikan "Connection failed". Jika koneksi berhasil dibuat, akan muncul pesan agar client mengetikkan sesuatu supaya server bisa mengkonfirmasi bahwa koneksi telah terjalin.

```
struct sockaddr_in alamatServer;
  char *login = "login", *logout = "logout", *regist = "register";
  char temp, buffer[1100] = {0}, cmd[1100], username[1100], password[1100];
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf(Failed to create socket);
    return -1;
  }
  memset(&alamatServer, '0', sizeof(alamatServer));
  alamatServer.sin_family = AF_INET, alamatServer.sin_port = htons(port);
  if (inet_pton(AF_INET, "127.0.0.1", &alamatServer.sin_addr) <= 0)
  {
    printf("Invalid address\n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&alamatServer, sizeof(alamatServer)) < 0)
  {
    printf("Connection failed\n");
    return -1;
  }
  readerConn = read(sock, buffer, 1100);
  printf("%s\n", buffer);
  while (!same("Connected", buffer))
  {
    printf("Enter anything to check whether you can connect to the server or not.\n");
    scanf("%s", cmd);
    send(sock, cmd, 1100, 0);
    readerConn = read(sock, buffer, 1100);
    printf("%s\n", buffer);
  }
  if (!same("Connected", buffer))
    return 0;
```

Setelah terjalin koneksi antara Client dan Server, akan muncul pilihan untuk Client melakukan login, register, atau exit. Fungsi scanf akan mengambil command dari client dan menjalankan fungsi selanjutnya sesuai dengan command yang diberikan oleh client.
```
  while (1)
  {
    printf("1. Login\n2. Register\n3. Exit\n");
    scanf("%s", cmd);
```

Jika client memasukkan command login, maka akan muncul pesan agar client memasukkan username dan password yang akan dikirimkan kepada server. Jika dikembalikan "LoginSuccess" dari server, client akan dapat masuk ke tahap berikutnya dan pesan bahwa login berhasil akan ditampilkan. Namun, jika tidak dikembalikan "LoginSuccess" dari server, akan ditampilkan pesan bahwa proses login telah gagal dan client dapat mencoba untuk memasukkan kembali informasinya.

```
  while (1)
  {
    if (same(cmd, login))
    {
      send(sock, login, strlen(login), 0);
      printf("Username: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      memset(buffer, 0, sizeof(buffer));
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "LoginSuccess"))
        printf("Login process is success! Welcome aboard! ^o^\n");
      else
        printf("Login process failed, please try again.\n");
```

Di sisi server, server akan membaca informasi login yang dikirimkan oleh klien dan membuka file user.txt dengan fopen mode "r" untuk membaca data user yang tersedia. Server akan membaca file pada user.txt dan melakukan compare terhadap informasi login yang telah diberikan oleh user untuk menentukan apakah user berhasil login atau tidak. Jika ditemukan informasi login yang valid, nilai check akan menjadi 1 dan server akan mengirimkan "LoginSuccess" sehingga client akan dapat melanjutkan ke tahap berikutnya.
```
while (1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (same(login, buffer))
    {
      readerConn = read(serverSocket, user.name, 1100);
      readerConn = read(serverSocket, user.password, 1100);
      fp3 = fopen("users.txt", "r");
      int check = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        char userName[105], userPass[105];
        int i = 0, j = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i++] = '\0';
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0 && strcmp(user.password, userPass) == 0)
        {
          check = 1, send(serverSocket, "LoginSuccess", strlen("LoginSuccess"), 0);
          break;
        }
      }
```

Namun, jika informasi login tidak sesuai dengan data yang dimiliki server, nilai check akan tetap 0 dan server akan mengirimkan "LoginFail" kepada client sehingga client harus memasukkan kembali informasi login.
```
    if (check == 0)
        printf("Authorization for Login is Failed\n"), send(serverSocket, "LoginFail", strlen("LoginFail"), 0);
      else
      {
        printf("Authorization for Login is Success\n");
```

Sedangkan jika client memasukkan command register, akan muncul pesan untuk memasukkan informasi registrasi yang terdiri dari username dan password. Client akan menggunakan fungsi scanf untuk membaca informasi tersebut dan mengirimkannya kepada server. Server akan mengembalikan "RegError" atau "Register Berhasil" tergantung kepada valid atau tidaknya informasi yang didapatkan. Jika diterima "RegError" dari server, akan ditampilkan pesan bahwa username sudah terambil atau password tidak sesuai ketentuan. Jika diterima "Register Berhasil" dari server, akan ditampilkan pesan bahwa proses registrasi sudah berhasil.

```
else if (same(cmd, regist))
    {
      send(sock, regist, strlen(regist), 0);
      memset(buffer, 0, sizeof(buffer));
      printf("Register\nUsername: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "RegError"))
        printf("Register gagal, username sudah terambil atau password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil.\n");
      if (same(buffer, "Register berhasil"))
        printf("Register berhasil\n");
      memset(buffer, 0, sizeof(buffer));
    }
```

Di sisi server, server akan membuka file user.txt dengan mode "a" dan "r" untuk melakukan append dan membaca file user.txt. Server akan menerima informasi registrasi kemudian mengecek dengan melakukan compare antara username yang ada di file.txt dan username yang diterima dari client. Jika tidak ditemukan perbedaan, maka username telah dipakai dan server akan melakukan break pada operasi dan mengembalikan "RegError". Jika pengecekan username berhasil dilewati tanpa break, server akan mengecek adanya minimal karakter, huruf kapital, huruf kecil, dan angka pada password dengan menggunakan nilai ASCII. Jika terdapat ketiganya, maka server akan melakukan append username dan password yang dimasukkan dalam file.txt dan mengembalikan "Register Berhasil" kepada client.

```
 else if (same(regist, buffer))
    {
      fp2 = fopen("users.txt", "a"), fp3 = fopen("users.txt", "r");
      readerConn = read(serverSocket, user.name, 1100),
      readerConn = read(serverSocket, user.password, 1100);
      int flag = 0;
      char *line = NULL;
      ssize_t len = 0;
      ssize_t fileReader;
      int check = 0, numUsers = 0;
      while ((fileReader = getline(&line, &len, fp3) != -1))
      {
        numUsers++;
        char userName[105], userPass[105];
        int i = 0;
        while (line[i] != ':')
          userName[i] = line[i], i++;
        userName[i] = '\0';
        i++;
        int j = 0;
        while (line[i] != '\n')
          userPass[j] = line[i], j++, i++;
        userPass[j] = '\0';
        if (strcmp(user.name, userName) == 0)
        {
          check = 0;
          break;
        }
        int kapital = 0, lower = 0, angka = 0, jumlah = 0;
        for (int i = 0; i < strlen(user.password); i++)
        {
          if (user.password[i] >= 48 && user.password[i] <= 57)
            angka = 1;
          if (user.password[i] >= 65 && user.password[i] <= 90)
            kapital = 1, jumlah++;
          if (user.password[i] >= 97 && user.password[i] <= 122)
            lower = 1, jumlah++;
        }
        if (!angka || !lower || !kapital || jumlah < 6)
          break;
        if (angka && lower && kapital && jumlah >= 6)
          check = 1;
      }
      if (check || numUsers == 0)
      {
        fprintf(fp2, "%s:%s\n", user.name, user.password);
        send(serverSocket, "Register berhasil", strlen("Register berhasil"), 0);
      }
      else
        send(serverSocket, "RegError", strlen("RegError"), 0);
      fclose(fp2);
    }
    else if (same("exit", buffer) || same("3", buffer))
    {
      close(serverSocket), total--;
      break;
    }
  }
```

## B. MEMBUAT DATABASE

Jika server berhasil dijalankan, akan dieksekusi fungsi fopen mode "a" yang akan membuat file problems.tsv jika sebelumnya file tersebut belum ada.
```
int main(int argc, char const *argv[])
{
  struct sockaddr_in alamat;
  int serverSocket, serverFD, readerConn, opt = 1, jumlahKoneksi = 5, porter = 8080, addr_len = sizeof(alamat);
  if ((serverFD = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    perror("socket failed"), exit(EXIT_FAILURE);
  if (setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    perror("setsockopt"), exit(EXIT_FAILURE);
  alamat.sin_family = AF_INET, alamat.sin_addr.s_addr = INADDR_ANY, alamat.sin_port = htons(porter);
  if (bind(serverFD, (struct sockaddr *)&alamat, sizeof(alamat)) < 0)
    perror("bind failed"), exit(EXIT_FAILURE);
  if (listen(serverFD, jumlahKoneksi) < 0)
    perror("listen"), exit(EXIT_FAILURE);
  mkdir("problems", 0777);
  FILE *fp = fopen("problems.tsv", "a");
```

User dapat menambahkan problem pada file ini dengan command add yang akan dijelaskan pada bagian C. File ini berisi judul problem dan username pembuat problem yang dipisahkan oleh \t sesuai perintah soal.
```
 fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
```

## C. ADD PROBLEM

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan add problem.
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

Jika client memasukkan command add, akan muncul pesan untuk memasukkan judul problem yang akan diambil menggunakan fungsi scanf dan dikirimkan kepada server. Akan muncul juga pesan untuk memasukkan filepath description.txt yang berisi deskripsi problem, filepath input.txt yang berisi input testcase untuk menyelesaikan problem, dan output.txt yang akan digunakan untuk melakukan pengecekan pada submission client.
```
  else if (same(cmd, "add"))
          {
            send(sock, cmd, 1100, 0);
            char data[1100], problemName[1100];
            printf("Judul Problem: ");
            scanf("\n%[^\n]%*c", problemName);
            send(sock, problemName, 1100, 0);
            printf("Filepath description.txt: ");
            scanf("\n%[^\n]%*c", data);
            char outputFolder[1100], inputFolder[1100], descFolder[1100], outputPath[1100];
            strcpy(descFolder, data);
            removeStr(descFolder, "/description.txt");
            mkdir(descFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath input.txt: ");
            scanf("\n%[^\n]%*c", data);
            strcpy(inputFolder, data);
            removeStr(inputFolder, "/input.txt");
            mkdir(inputFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath output.txt: ");
            scanf("\n%[^\n]%*c", data);
            send(sock, data, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numGacha = atoi(buffer);
            memset(buffer, 0, sizeof(buffer));
            strcpy(outputPath, data);
            FILE *fileManager;
            fileManager = fopen(outputPath, "a+");
            for (int i = 0; i < numGacha; i++)
              readerConn = read(sock, buffer, 1100),
              fprintf(fileManager, "%s", buffer),
              memset(buffer, 0, sizeof(buffer));
            fclose(fileManager);
            printf("Successfully adding problem %s to the server!\n", problemName);
            continue;
          }
```

Di sisi server, server akan membaca kiriman informasi dari client dan mengirimakn request untuk judul problem, path description, path input, dan path output. Server kemudian membuat direktori yang mengambil nama dari input judul problem yang dimasukkan oleh client dan menyimpan problem di dalamnya dengan strcpy dan strcat. Setelah itu, akan ditampilkan pesan "(username) is adding new problem called (judul) to the server".
```
 else if (same("add", buffer))
          {
            problem request;
            char basePath[1100], clientPath[1100], descPath[1100], inputPath[1100];
            char descServer[1100], inputServer[1100], outputServer[1100];
            readerConn = read(serverSocket, request.problem_title, 1100);
            printf("Judul problem: %s\n", request.problem_title);
            readerConn = read(serverSocket, request.pathDesc, 1100);
            printf("Filepath description.txt: %s\n", request.pathDesc);
            readerConn = read(serverSocket, request.pathInput, 1100);
            printf("Filepath input.txt: %s\n", request.pathInput);
            readerConn = read(serverSocket, request.pathOutput, 1100);
            printf("Filepath output.txt: %s\n", request.pathOutput);
            strcpy(basePath, "problems/");
            strcat(basePath, request.problem_title);
            mkdir(basePath, 0777);
            strcat(basePath, "/");
            strcpy(descServer, basePath);
            strcat(descServer, "description.txt");
            strcpy(inputServer, basePath);
            strcat(inputServer, "input.txt");
            strcpy(outputServer, basePath);
            strcat(outputServer, "output.txt");
            fp = fopen("problems.tsv", "a");
            fprintf(fp, "%s\t%s\n", request.problem_title, user.name), fclose(fp);
            printf("%s is adding new problem called %s to the server.\n", user.name, request.problem_title);
            continue;
          }
```

## D. SEE PROBLEM

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan see problem.
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

Jika user memasukkan command see, akan ditampilkan pesan berupa "Problems Available in Online Judge" dan client akan mengirimkan perintah "see" kepada server. Perintah ini akan membuat server mengembalikan daftar judul dan pembuat problem.
```
 else if (same(cmd, "see"))
          {
            printf("Problems Available in Online Judge:\n");
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            while (readerConn = read(sock, buffer, 1100))
            {
              if (same(buffer, "e"))
                break;
              printf("%s", buffer);
            }
            continue;
```

Di sisi server, jika server mendapatkan command "see", server akan membuka file problems.tsv dengan fungsi fopen mode "r" untuk membaca problem dan mengirimkan line demi line berisi judul problem dan pengarangnya kepada client sebelum menutup file tersebut dengan fclose.

```
      else if (same("see", buffer))
          {
            fp = fopen("problems.tsv", "r");
            if (!fp)
            {
              send(serverSocket, "e", sizeof("e"), 0);
              memset(buffer, 0, sizeof(buffer));
              continue;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            while ((fileReader = getline(&line, &len, fp) != -1))
            {
              problem temp_problem;
              storeProblemTSV(&temp_problem, line);
              char message[1100];
              sprintf(message, "%s by %s", temp_problem.problem_title, temp_problem.author);
              send(serverSocket, message, 1100, 0);
            }
            send(serverSocket, "e", sizeof("e"), 0), fclose(fp);
            memset(buffer, 0, sizeof(buffer));
          }
        }
```

## E. Download Problem

Setelah client berhasil melakukan login, client akan mendapatkan beberapa pilihan. Salah satunya adalah untuk melakukan download problem.
```
 if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
```

Jika client memasukkan command download [judul-problem], client akan mengirimkan command tersebut menggunakan fungsi send kepada server. Client akan membuka file input target menggunakan fopen untuk membuat file tersebut jika belum ada dan mengeprint isi file yang dikirimkan oleh server ke dalam file input target sebelum menutupnya ketika tidak ada line yang dapat diprint lagi. Client kemudian membaca respon dari server dan akan menampilkan pesan bahwa file sedang didownload. Client akan memindahkan kiriman dari server ke dalam buffer dan menuliskannya ke dalam destinasi. Setelah itu, server akan menampilkan pesan bahwa file telah berhasil didownload.
```
          else if (same(cmd, "download"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            scanf("%s", cmd);
            send(sock, cmd, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numLine = atoi(buffer);
            char inputTarget[1100];
            readerConn = read(sock, inputTarget, 1100);
            for (int i = 0; i < numLine; i++)
            {
              FILE *fpClient = fopen(inputTarget, "r");
              fpClient = fopen(inputTarget, "a+");
              char fileContent[1100];
              readerConn = read(sock, fileContent, 1100);
              fprintf(fpClient, "%s\n", fileContent), fclose(fpClient);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            char fileReady[] = "File is ready to be downloaded.\n", descTarget[1100];
            if (same(buffer, fileReady))
            {
              memset(buffer, 0, sizeof(buffer));
              readerConn = read(sock, buffer, 1100);
              strcpy(descTarget, buffer);
              printf("Downloading description & input from problem %s..\n", cmd);
              int des_fd = open(descTarget, O_WRONLY | O_CREAT | O_EXCL, 0700);
              if (!des_fd)
                perror("can't open file"), exit(EXIT_FAILURE);
              int file_read_len;
              char buff[1100];
              while (1)
              {
                memset(buff, 0, 1100);
                file_read_len = read(sock, buff, 1100);
                write(des_fd, buff, file_read_len);
                break;
              }
              printf("Successfully downloading the files for problem %s to the desired client directory!\n", cmd);
            }
            memset(buffer, 0, sizeof(buffer));
            continue;
```

Di sisi server, server akan memeriksa apakah kiriman [judul-problem] dari client tersedia menggunakan strcpy dan strcat. Jika ditemukan, program akan menampilkan pesan bahwa file siap didownload dan jika tidak, akan ditampilkan pesan bahwa file tidak ditemukan. Program akan membuka file pada destinasi yang diminta oleh client dan menggunakan loop untuk mengirimkan isinya kepada client sebelum menutup file tersebut.
```
          else if (same("download", buffer))
          {
            readerConn = read(serverSocket, buffer, 1100);
            bool foundDesc = false, foundInput = false;
            char pathDownload[1100] = "problems/", descPath[1100], descTarget[1100];
            strcat(pathDownload, buffer);
            strcpy(descPath, pathDownload);
            strcat(descPath, "/description.txt");
            char inputPath[1100], inputTarget[1100], outputPath[1100], outputTarget[1100];
            strcpy(inputPath, pathDownload);
            strcat(inputPath, "/input.txt");
            strcpy(outputPath, pathDownload);
            strcat(outputPath, "/output.txt");
            char foundSuccess[] = "File is ready to be downloaded.\n", notFound[] = "No such file found.\n";
            fp = fopen(descPath, "r");
            FILE *source, *target;
            source = fopen(descPath, "r");
            char *line = NULL, *line2 = NULL, *line3 = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            int num = 0, fd, readLength;
            while ((fileReader = getline(&line, &len, source) != -1))
            {
              num++;
              if (num > 3)
              {
                char pathContent[105];
                int i = 0, j = 0;
                while (line[i] != ':')
                  i++;
                line[i] = '\0';
                i += 2;
                while (line[i] != '\n')
                  pathContent[j++] = line[i++];
                pathContent[j] = '\0';
                if (num == 4)
                  strcpy(descTarget, pathContent);
                if (num == 5)
                  strcpy(inputTarget, pathContent);
                if (num == 6)
                  strcpy(outputTarget, pathContent);
              }
            }
            int numLine = 0;
            FILE *fpSource = fopen(inputPath, "r");
            ssize_t len2 = 0;
            ssize_t fileReader2;
            bool checkSubmission = true;
            while ((fileReader2 = getline(&line2, &len2, fpSource) != -1))
              numLine++;
            fclose(fpSource);
            fpSource = fopen(inputPath, "r");
            char buf[1100];
            sprintf(buf, "%d", numLine);
            send(serverSocket, buf, 1100, 0);
            send(serverSocket, inputTarget, 1100, 0);
            ssize_t len3 = 0;
            ssize_t fileReader3;
            while ((fileReader3 = getline(&line3, &len3, fpSource) != -1))
            {
              char outputSource[105];
              int i = 0;
              while (line3[i] != '\n')
                outputSource[i] = line3[i], i++;
              outputSource[i] = '\0';
              send(serverSocket, outputSource, 1100, 0);
            }
            fclose(fpSource);
            if (fp)
              foundDesc = true;
            if (!foundDesc)
              send(serverSocket, notFound, 1100, 0);
            else
            {
              send(serverSocket, foundSuccess, 1100, 0);
              send(serverSocket, descTarget, 1100, 0);
              fd = open(descPath, O_RDONLY);
              if (!fd)
                perror("Fail to open"), exit(EXIT_FAILURE);
              while (1)
              {
                memset(descPath, 0x00, 1100);
                readLength = read(fd, descPath, 1100);
                if (readLength == 0)
                  break;
                else
                  send(serverSocket, descPath, readLength, 0);
              }
              close(fd);
            }
            fclose(fp);
          }
```


## F. Submit Problem

Jika client memasukkan command submit [judul-problem] [path-file-output], client akan mengirim command tersebut kepada server kemudian memasukkan [judul-problem] dan [path-file-output] untuk dikirimkan juga kepada server. Client kemudian akan mencoba untuk membuka file dan membacanya. Jika file tidak tersedia, akan dikembalikan pesan "Output file is not exist!" dan break akan terjadi. Jika file tersedia, program akan masuk ke dalam loop untuk mengambil line demi line dari output submission menggunakan fungsi getline untuk dikirimkan kepada server. Setelah tidak ada line untuk diambil lagi, program akan membaca perintah yang dikembalikan oleh server dan menampilkan hasil dari submission.
```
          else if (same(cmd, "submit"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            char namaProblem[1100], filePath[1100];
            scanf("%s %s", namaProblem, filePath);
            send(sock, namaProblem, 1100, 0);
            FILE *fpC = fopen(filePath, "r");
            if (!fpC)
            {
              printf("Output file is not exist!\n");
              break;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while ((fileReader = getline(&line, &len, fpC) != -1))
            {
              char outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSubmission[j++] = line[i++];
              outputSubmission[j] = '\0';
              send(sock, outputSubmission, 105, 0);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
          }
```


Di sisi server, server akan membaca informasi [judul-problem] [path-file-output] dari soal dan membuka file yang berada di destinasi path yang diberikan. Server kemudian mengambil line demi line menggunakan loop untuk membandingkan antara isi dari output yang disubmit oleh client dan output yang tersedia di database. Jika yang disubmit tidak sama dengan yang ada di database server, akan dikembalikan false kepada client.
```
          else if (same("submit", buffer))
          {
            char problemName[1100], pathSource[1100] = "problems/";
            readerConn = read(serverSocket, problemName, 1100);
            strcat(pathSource, problemName);
            strcat(pathSource, "/output.txt");
            FILE *fpSource = fopen(pathSource, "r");
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while (fileReader = getline(&line, &len, fpSource) != -1)
            {
              char outputSource[105], outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSource[j++] = line[i++];
              outputSource[j] = '\0';
              readerConn = read(serverSocket, outputSubmission, 105);
              int source = atoi(outputSource), submit = atoi(outputSubmission);
              printf("[%s] source: %d dan submission: %d\n", submit == source ? "AC" : "WA", source, submit);
              if (submit != source)
                checkSubmission = false;
            }
            printf("%s is %s on problem %s!\n", user.name, checkSubmission ? "accepted (AC)" : "having a wrong answer (WA)", problemName);
            send(serverSocket, checkSubmission ? "AC\nAccepted in Every Testcases\n" : "WA\nWA in the First Testcase\n", 1100, 0);
          }
```


## G. Multiple Connection

Program menangani multiple connection dengan mengecek berapa banyak client yang terkoneksi. Jika terdapat tepat satu client, server akan mengirimkan pesan connected kepada client, sedangkan jika tidak maka server akan mengirimkan pesan failed. Selama terbaca bahwa total client lebih dari satu, server akan mengecek banyak client yang terhubung. Jika setelah itu jumlah client adalah satu, maka server akan mengirimkan pesan connected. Namun, jika tidak, server akan mengirimkan pesan failed. Failed akan menampilkan pada client yang gagal terhubung bahwa ada client yang sedang terkoneksi dan untuk menunggu koneksi diakhiri.
```
  char buffer[1100] = {0}, connected[1100] = "Connected", failed[1100] = "Other people are connected, please wait for them ^o^";
  int readerConn, serverSocket = *(int *)tmp;
  if (total == 1)
    send(serverSocket, connected, 1100, 0);
  else
    send(serverSocket, failed, 1100, 0);
  while (total > 1)
  {
    readerConn = read(serverSocket, buffer, 1100);
    if (total == 1)
      send(serverSocket, connected, 1100, 0);
    else
      send(serverSocket, failed, 1100, 0);
  }
```
 
# SOAL 3

- Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

Contoh jika program pengkategorian dijalankan

```
# Program soal3 terletak di /home/[user]/shift3/hartakarun
$ ./soal3 
# Hasilnya adalah sebagai berikut
/home/[user]/shift3/hartakarun
|-jpg
|--file1.jpg
|--file2.jpg
|--file3.jpg
|-c
|--file1.c
|-tar.gz
|--file1.tar.gz

```

- a. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif


Code :
```c
void listFilesRecursively(char *basePath)
{
   char path[1000];
   struct dirent *dp;
   DIR *dir = opendir(basePath);
 
   if (!dir)
       return;
 
   while ((dp = readdir(dir)) != NULL)
   {
       if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
       {
           char destDir[1000];
           strcpy(destDir,basePath);
           strcat(destDir,"/");
           strcat(destDir,dp->d_name);
           printf("%s\n",destDir);
         
           if(checkIfFileExists(destDir)){
               strcpy(save[indeks],destDir);
               indeks+=1;
           }
           // Construct new path from our base path
           strcpy(path, basePath);
           strcat(path, "/");
           strcat(path, dp->d_name);
 
           listFilesRecursively(path);
       }
   }
 
   closedir(dir);
}
 
void* moveFile(void* argc){
   char* src=(char*) argc;
 
   //pindah file
   char file[1000];
   strcpy(file,src);
   //printf("%s",file);
     
   char *ndir,*dir;
   char x='/'; 
   char y='.';
   dir = strrchr(file,x); //yg pertamakali dijumpai
   ndir = strchr(file, y);
   char ext[1000];
   //printf("%s",dir);
   if (dir){
       //ngecek file/dir/ada ngga
       if(checkIfFileExists(file)){
           cekExt(dir+1,ext);
       }
       else{
           return 0;
       }
   }
   mkdir(ext,0777);//extension dir
   //src
   char path[1000];
   strcpy(path,(char*) argc);
   //dst
   char fileCat[1000];
   getcwd(fileCat,sizeof(path));
   strcat(fileCat,"/");
   strcat(fileCat,ext);
   strcat(fileCat,"/");
   strcat(fileCat,dir+1);
   printf("%s\n%s\n",path,fileCat);
   rename(path,fileCat);
  
   return(void *) 1;
   //moves(src);
   pthread_exit(0);
}
```

Kode untuk mengkategorikan file secara rekursif dan memindahkannya kedalam folder sesuai ekstensi. Fungsi listFilesRecursively untuk mengkategorikan secara rekursif dan fungsi *moveFile untuk memindahkannya.


- b. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

```c
void cekExt(char* fileName,char *ext){
   char *extt=strchr(fileName,'.'); //kalau ada 2 ext->ambil paling depan
   if(extt==fileName){
       strcpy(ext,"Hidden");
   }
   else if (extt==NULL){
       strcpy(ext,"Unknown");
   }
   else{
       strcpy(ext,extt+1);
       for(int x=0;x<strlen(ext);x++){
           //ubah jadi lowercase
           ext[x]=tolower(ext[x]);
       }
   }
}
```

Dengan fungsi cekExt kita akan memeriksa ekstensi dari tiap-tiap file. Selain itu fungsi ini juga akan memeriksa file tanpa ekstensi dan file yang tersembunyi atau Hidden.

- c. Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread

```c
   pthread_t tid2[indeks];
   printf("%d",indeks);
   int i=0,j=0;
   while(i<indeks){
       printf("%s\n",save[i]);
       pthread_create(&tid2[i],NULL, moveFile,(void *)save[i]);
       i++;
   }
   while (j<indeks)
   {
       void *ptr;
       pthread_join(tid2[j],&ptr);
       j++;
   }
```

Potongan kode untuk melakukan thread pada tiap ekstensi file.

# Kesulitan
1. Masih belum menemukan cara untuk memindahkan file-file ke dalam folder sesuai ekstensinya
2. Mengalami kesulitan ketika memindahkan file secara rekursif 1
3. Masih belum mencoba membuat file client dan server
