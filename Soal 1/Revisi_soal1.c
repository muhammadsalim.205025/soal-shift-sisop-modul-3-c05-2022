#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <dirent.h>
#include <memory.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void download_drive(char *id, char *save)
{
    char *link = malloc(100);
    strcpy(link, "https://docs.google.com/uc?export=download&id=");
    strcat(link, id);
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"wget", "--quiet", "--no-check-certificate", link, "-O", save, NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    wait(&status);
}


void create_dir(char *dirname)
{
    pid_t pid = fork();
    int status;
    if (pid == 0)
    {
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    wait(&status);
}


char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char *base64_decode(char *encoded)
{
    char counts = 0;
    char buffer[4];
    char *decoded = malloc(strlen(encoded) * 3 / 4);
    int i = 0, p = 0;

    for (i = 0; encoded[i] != '\0'; i++)
    {
        char k;
        for (k = 0; k < 64 && base64_map[k] != encoded[i]; k++)
            ;
        buffer[counts++] = k;
        if (counts == 4)
        {
            decoded[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                decoded[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                decoded[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    decoded[p] = '\0';
    return decoded;
}

pthread_t tid[3];
pthread_t tid2[3];

void *thread_unzip(void *arg)
{
    char *argv1[] = {"unzip", "-qo", "music.zip", "-d", "./music/", NULL};
    char *argv2[] = {"unzip", "-qo", "quote.zip", "-d", "./quote/", NULL};
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[0]))
    {
        if (fork() == 0)
        {
            execv("/usr/bin/unzip", argv1);
        }
        wait(NULL);
    }
    else if (pthread_equal(id, tid[1]))
    {
        if (fork() == 0)
        {
            execv("/usr/bin/unzip", argv2);
        }
        wait(NULL);
    }
    return NULL;
}

void *thread_decode(void *arg)
{
    pthread_t id = pthread_self();
    char path1[10];
    char path2[12];
    char filename[10];
    if (pthread_equal(id, tid2[0]))
    {
        strcpy(path1, "./music/");
        strcpy(path2, "./music/%s");
        strcpy(filename, "music.txt");
    }
    else
    {
        strcpy(path1, "./quote/");
        strcpy(path2, "./quote/%s");
        strcpy(filename, "quote.txt");
    }
    DIR *dir;
    struct dirent *dp;
    dir = opendir(path1);
    FILE *file, *txt;

    if (dir != NULL)
    {
        while ((dp = readdir(dir)))
        {
            char str[100] = "";
            char fullpath[1000] = "";
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                sprintf(fullpath, path2, dp->d_name);
                file = fopen(fullpath, "r");
                while (fgets(str, 100, file) != NULL)
                {
                    txt = fopen(filename, "a");
                    fprintf(txt, "%s\n", base64_decode(str));
                    fclose(txt);
                }
                fclose(file);
            }
        }

        (void)closedir(dir);
    }
    else
        perror("Tidak bisa membuka directory");
    return NULL;
}

int main(void)
{
  
    download_drive("1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "music.zip");
    download_drive("1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "quote.zip");

    create_dir("music");
    create_dir("quote");

    sleep(10);
    int i = 0;
    int j = 0;
    int err;
    while (i < 2)
    {
        err = pthread_create(&(tid[i]), NULL, &thread_unzip, NULL);
        if (err != 0)
            printf("\n Gagal membuat thread : [%s]", strerror(err));
        else
            printf("\n Berhasil membuat thread untuk unzip\n");
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    sleep(5);
    
    while (j < 2)
    {
        err = pthread_create(&(tid2[j]), NULL, &thread_decode, NULL);
        if (err != 0)
        {
            printf("\n Gagal membuat thread : [%s]", strerror(err));
        }
        else
        {
            printf("\n Berhasil membuat thread untuk decode\n");
        }
        j++;
    }
    pthread_join(tid2[0], NULL);
    pthread_join(tid2[1], NULL);

    
    create_dir("hasil");

    if (fork() == 0)
    {
        char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    wait(NULL);

    if (fork() == 0)
    {
        char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    wait(NULL);

    exit(0);
    return (0);
}