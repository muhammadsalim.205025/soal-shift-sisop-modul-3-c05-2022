#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h> 
#include <sys/stat.h>
#include <ctype.h>
#include<dirent.h>
 
char dest[1000];
char save[1000][1000];
int indeks=0;
 
int isRegular(const char *path){ //cek file/dir
   struct stat path_stat;
   stat(path,&path_stat);
   return S_ISREG(path_stat.st_mode);
}
 
//cek file ada atau ngga
int checkIfFileExists(const char * filename)
{
   FILE *file;
   file = fopen(filename, "rb");
   if (file!=NULL)
   {
       // printf("%s\n",filename);
       fclose(file);
       if(isRegular(filename)) return 1; //cek file
       else return 0;
   }
 
   return 0;
}
 
//cek extension
void cekExt(char* fileName,char *ext){
   char *extt=strchr(fileName,'.'); //kalau ada 2 ext->ambil paling depan
   if(extt==fileName){
       strcpy(ext,"Hidden");
   }
   else if (extt==NULL){
       strcpy(ext,"Unknown");
   }
   else{
       strcpy(ext,extt+1);
       for(int x=0;x<strlen(ext);x++){
           //ubah jadi lowercase
           ext[x]=tolower(ext[x]);
       }
   }
}
 
void listFilesRecursively(char *basePath)
{
   char path[1000];
   struct dirent *dp;
   DIR *dir = opendir(basePath);
 
   if (!dir)
       return;
 
   while ((dp = readdir(dir)) != NULL)
   {
       if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
       {
           char destDir[1000];
           strcpy(destDir,basePath);
           strcat(destDir,"/");
           strcat(destDir,dp->d_name);
           printf("%s\n",destDir);
         
           if(checkIfFileExists(destDir)){
               strcpy(save[indeks],destDir);
               indeks+=1;
           }
           // Construct new path from our base path
           strcpy(path, basePath);
           strcat(path, "/");
           strcat(path, dp->d_name);
 
           listFilesRecursively(path);
       }
   }
 
   closedir(dir);
}
 
void* moveFile(void* argc){
   char* src=(char*) argc;
 
   //pindah file
   char file[1000];
   strcpy(file,src);
   //printf("%s",file);
     
   char *ndir,*dir;
   char x='/'; 
   char y='.';
   dir = strrchr(file,x); //yg pertamakali dijumpai
   ndir = strchr(file, y);
   char ext[1000];
   //printf("%s",dir);
   if (dir){
       //ngecek file/dir/ada ngga
       if(checkIfFileExists(file)){
           cekExt(dir+1,ext);
       }
       else{
           return 0;
       }
   }
   mkdir(ext,0777);//extension dir
   //src
   char path[1000];
   strcpy(path,(char*) argc);
   //dst
   char fileCat[1000];
   getcwd(fileCat,sizeof(path));
   strcat(fileCat,"/");
   strcat(fileCat,ext);
   strcat(fileCat,"/");
   strcat(fileCat,dir+1);
   printf("%s\n%s\n",path,fileCat);
   rename(path,fileCat);
  
   return(void *) 1;
   //moves(src);
   pthread_exit(0);
}
 
int main(int argc, char* argv[]){
  
   char src[1000];
   chdir("hartakarun");
   getcwd(dest,sizeof(dest));
   strcpy(src,dest);
 
   pthread_t tid[argc-2];
   char dir[100];

   strcpy(dir,src);

   listFilesRecursively(dir);

   pthread_t tid2[indeks];
   printf("%d",indeks);
   int i=0,j=0;
   while(i<indeks){
       printf("%s\n",save[i]);
       pthread_create(&tid2[i],NULL, moveFile,(void *)save[i]);
       i++;
   }
   while (j<indeks)
   {
       void *ptr;
       pthread_join(tid2[j],&ptr);
       j++;
   }

   return 0;
}


